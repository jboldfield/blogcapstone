package com.sg.blogcapstone.dao;

import com.sg.blogcapstone.TestApplicationConfiguration;
import com.sg.blogcapstone.entity.Post;
import com.sg.blogcapstone.entity.Tag;
import com.sg.blogcapstone.entity.User;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author joldf
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApplicationConfiguration.class)
public class PostDaoDBTest {
    
    @Autowired
    PostDao postDao;
    
    @Autowired
    UserDao userDao;
    
    public PostDaoDBTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        List<Tag> tags = postDao.getAllTags();
        tags.forEach(tag -> {
            postDao.deleteTagById(tag.getTagId());
        });
        
        List<Post> posts = postDao.getAllPosts();
        posts.forEach(post -> {
            postDao.deletePostById(post.getPostId());
        });
        
        List<User> users = userDao.getAllUsers();
        users.forEach(user -> {
            userDao.deleteUserById(user.getUserId());
        });
    }
    
    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testAddPostAndGetPostById() {
        User user = new User();
        user.setFirstName("Site");
        user.setLastName("Owner");
        user.setEmail("owner@blog.com");
        user.setRole("Admin");
        user.setPassword("password");
        user = userDao.addUser(user);
        
        Post post = new Post();
        post.setUserId(user.getUserId());
        post.setTitle("Blog 1");
        post.setContent("Lorem ipsum dolor sit amet, pri te integre inermis torquatos, eu mel simul nostrud repudiare, et sanctus legendos his. At has porro similique. No sententiae assueverit mei, ius in integre legimus propriae. Sea quod facete quaerendum ne.\n" +
                "\n" + "Ius amet idque civibus an. Debet aperiam te nec. Ei admodum platonem sea, duo id constituto referrentur. Nec nostrum senserit an. Agam oblique dolores ne mei, dicat mazim adversarium an est.\n" +
                "\n" + "Sit patrioque efficiantur no, sale apeirian ut eam. Minim altera eu nec, fugit accusam tractatos quo id. Nam facer mucius impetus ei. Eum eu oblique pertinax, vim ea diam inermis insolens. Sea utroque nominati ad, cu principes interpretaris eam, mel et malis semper praesent.\n" +
                "\n" + "Ea exerci civibus mei. Ut nam primis antiopam, vel illum nostrud reprimique ea, nusquam volutpat duo no. Pro hinc erroribus scriptorem at, id mel dicam volutpat euripidis. Luptatum rationibus ut pro, adhuc moderatius appellantur an duo. Dicta elitr petentium est ne, cu noluisse invenire delicata vix. Mel ne debet quidam facilis, id pri ipsum saperet.\n" +
                "\n" + "Dolorem mediocrem dissentiunt in has. Mea ut reque persequeris, an ius sensibus platonem, eirmod dolores eam eu. No quo mazim adipisci voluptatum. Eu vel dicta nominavi corrumpit. Mei cu graece intellegat. Ei iisque.");
        post.setExpirationDate(new Date(2021, 12, 30));
        post.setApproved(true);
        post = postDao.addPost(post);

        Post newPost = postDao.getPostById(post.getPostId());
        
        assertEquals(post, newPost);
    }
    
    @Test
    public void testGetAllPosts() {
        User user = new User();
        user.setFirstName("Site");
        user.setLastName("Owner");
        user.setEmail("owner@blog.com");
        user.setRole("Admin");
        user.setPassword("password");
        user = userDao.addUser(user);
        
        Post post = new Post();
        post.setUserId(user.getUserId());
        post.setTitle("Blog 1");
        post.setContent("Lorem ipsum dolor sit amet, pri te integre inermis torquatos, eu mel simul nostrud repudiare, et sanctus legendos his. At has porro similique. No sententiae assueverit mei, ius in integre legimus propriae. Sea quod facete quaerendum ne.\n" +
                "\n" + "Ius amet idque civibus an. Debet aperiam te nec. Ei admodum platonem sea, duo id constituto referrentur. Nec nostrum senserit an. Agam oblique dolores ne mei, dicat mazim adversarium an est.\n" +
                "\n" + "Sit patrioque efficiantur no, sale apeirian ut eam. Minim altera eu nec, fugit accusam tractatos quo id. Nam facer mucius impetus ei. Eum eu oblique pertinax, vim ea diam inermis insolens. Sea utroque nominati ad, cu principes interpretaris eam, mel et malis semper praesent.\n" +
                "\n" + "Ea exerci civibus mei. Ut nam primis antiopam, vel illum nostrud reprimique ea, nusquam volutpat duo no. Pro hinc erroribus scriptorem at, id mel dicam volutpat euripidis. Luptatum rationibus ut pro, adhuc moderatius appellantur an duo. Dicta elitr petentium est ne, cu noluisse invenire delicata vix. Mel ne debet quidam facilis, id pri ipsum saperet.\n" +
                "\n" + "Dolorem mediocrem dissentiunt in has. Mea ut reque persequeris, an ius sensibus platonem, eirmod dolores eam eu. No quo mazim adipisci voluptatum. Eu vel dicta nominavi corrumpit. Mei cu graece intellegat. Ei iisque.");
        post.setExpirationDate(new Date(2021, 12, 30));
        post.setApproved(true);
        post = postDao.addPost(post);
        
        Post post2 = new Post();
        post2.setUserId(user.getUserId());
        post2.setTitle("Expired Blog");
        post2.setContent("Lorem ipsum dolor sit amet, in vitae nostro mea, et mel mollis noluisse signiferumque, at sanctus dolorem corpora pri. Vitae singulis theophrastus ad duo, pri odio soleat an, vis vocibus periculis ut. His in dicat accusamus, eu has partem quodsi. Facer animal intellegat pri ea.\n" +
                "\n" + "Everti gubergren ius ex, et dicunt senserit pri. Duo et aliquam platonem suavitate, omnis facilisi antiopam ex vis, explicari moderatius eum ei. Vix reque consetetur in, cu idque adversarium duo, deserunt reprimique necessitatibus ad has. Scaevola persecuti cu vis. Velit possit semper eos an, doctus scribentur in sit, ius harum facilisis percipitur an.\n" +
                "\n" + "Eum omnis semper et. Dolore principes necessitatibus id mei, sea nisl iisque mandamus ut, perpetua sadipscing ad pri. Id per vocibus scaevola, vim elit civibus eu, in fugit neglegentur usu. Eum eu persius adipiscing, an eam legere essent, mea iriure eruditi no. No etiam melius appetere pri.\n" +
                "\n" + "Est ne solet quaeque, quo eirmod ponderum dissentias an. His no modus senserit, quo an fabulas iudicabit, in dicat dicta veritus cum. Sea illud disputando repudiandae ne. Choro utamur praesent ut his.\n" +
                "\n" + "Corpora invidunt ea pro, nobis platonem ullamcorper ut mea. His ad quis ocurreret, per te erant persecuti. Ex diam eirmod per. Nec at.");
        post2.setExpirationDate(new Date(2021, 11, 30));
        post2.setApproved(true);
        post2 = postDao.addPost(post2);
        
        List<Post> posts = postDao.getAllPosts();
        
        assertEquals(2, posts.size());
        assertTrue(posts.contains(post));
        assertTrue(posts.contains(post2));
    }
    
    @Test
    public void testUpdateBlog(){
        User user = new User();
        user.setFirstName("Site");
        user.setLastName("Owner");
        user.setEmail("owner@blog.com");
        user.setRole("Admin");
        user.setPassword("password");
        user = userDao.addUser(user);
        
        Post post = new Post();
        post.setUserId(user.getUserId());
        post.setTitle("Blog 1");
        post.setContent("Lorem ipsum dolor sit amet, pri te integre inermis torquatos, eu mel simul nostrud repudiare, et sanctus legendos his. At has porro similique. No sententiae assueverit mei, ius in integre legimus propriae. Sea quod facete quaerendum ne.\n" +
                "\n" + "Ius amet idque civibus an. Debet aperiam te nec. Ei admodum platonem sea, duo id constituto referrentur. Nec nostrum senserit an. Agam oblique dolores ne mei, dicat mazim adversarium an est.\n" +
                "\n" + "Sit patrioque efficiantur no, sale apeirian ut eam. Minim altera eu nec, fugit accusam tractatos quo id. Nam facer mucius impetus ei. Eum eu oblique pertinax, vim ea diam inermis insolens. Sea utroque nominati ad, cu principes interpretaris eam, mel et malis semper praesent.\n" +
                "\n" + "Ea exerci civibus mei. Ut nam primis antiopam, vel illum nostrud reprimique ea, nusquam volutpat duo no. Pro hinc erroribus scriptorem at, id mel dicam volutpat euripidis. Luptatum rationibus ut pro, adhuc moderatius appellantur an duo. Dicta elitr petentium est ne, cu noluisse invenire delicata vix. Mel ne debet quidam facilis, id pri ipsum saperet.\n" +
                "\n" + "Dolorem mediocrem dissentiunt in has. Mea ut reque persequeris, an ius sensibus platonem, eirmod dolores eam eu. No quo mazim adipisci voluptatum. Eu vel dicta nominavi corrumpit. Mei cu graece intellegat. Ei iisque.");
        post.setExpirationDate(new Date(2021, 12, 30));
        post.setApproved(true);
        Post originalPost = postDao.addPost(post);
        
        post.setTitle("New Blog Title");
        postDao.updatePost(post);
                
        assertNotEquals(originalPost, postDao.getPostById(post.getPostId()));
    }
    
    @Test
    public void testDeleteBlog(){
        User user = new User();
        user.setFirstName("Site");
        user.setLastName("Owner");
        user.setEmail("owner@blog.com");
        user.setRole("Admin");
        user.setPassword("password");
        user = userDao.addUser(user);
        
        Post post = new Post();
        post.setUserId(user.getUserId());
        post.setTitle("Blog 1");
        post.setContent("Lorem ipsum dolor sit amet, pri te integre inermis torquatos, eu mel simul nostrud repudiare, et sanctus legendos his. At has porro similique. No sententiae assueverit mei, ius in integre legimus propriae. Sea quod facete quaerendum ne.\n" +
                "\n" + "Ius amet idque civibus an. Debet aperiam te nec. Ei admodum platonem sea, duo id constituto referrentur. Nec nostrum senserit an. Agam oblique dolores ne mei, dicat mazim adversarium an est.\n" +
                "\n" + "Sit patrioque efficiantur no, sale apeirian ut eam. Minim altera eu nec, fugit accusam tractatos quo id. Nam facer mucius impetus ei. Eum eu oblique pertinax, vim ea diam inermis insolens. Sea utroque nominati ad, cu principes interpretaris eam, mel et malis semper praesent.\n" +
                "\n" + "Ea exerci civibus mei. Ut nam primis antiopam, vel illum nostrud reprimique ea, nusquam volutpat duo no. Pro hinc erroribus scriptorem at, id mel dicam volutpat euripidis. Luptatum rationibus ut pro, adhuc moderatius appellantur an duo. Dicta elitr petentium est ne, cu noluisse invenire delicata vix. Mel ne debet quidam facilis, id pri ipsum saperet.\n" +
                "\n" + "Dolorem mediocrem dissentiunt in has. Mea ut reque persequeris, an ius sensibus platonem, eirmod dolores eam eu. No quo mazim adipisci voluptatum. Eu vel dicta nominavi corrumpit. Mei cu graece intellegat. Ei iisque.");
        post.setExpirationDate(new Date(2021, 12, 30));
        post.setApproved(true);
        post = postDao.addPost(post);
        
        postDao.deletePostById(post.getPostId());
                
        assertNull(postDao.getPostById(post.getPostId()));
    }
    
    @Test
    public void testAddTagAndGetTagById() {
        User user = new User();
        user.setFirstName("Site");
        user.setLastName("Owner");
        user.setEmail("owner@blog.com");
        user.setRole("Admin");
        user.setPassword("password");
        user = userDao.addUser(user);
        
        Post post = new Post();
        post.setUserId(user.getUserId());
        post.setTitle("Blog 1");
        post.setContent("Lorem ipsum dolor sit amet, pri te integre inermis torquatos, eu mel simul nostrud repudiare, et sanctus legendos his. At has porro similique. No sententiae assueverit mei, ius in integre legimus propriae. Sea quod facete quaerendum ne.\n" +
                "\n" + "Ius amet idque civibus an. Debet aperiam te nec. Ei admodum platonem sea, duo id constituto referrentur. Nec nostrum senserit an. Agam oblique dolores ne mei, dicat mazim adversarium an est.\n" +
                "\n" + "Sit patrioque efficiantur no, sale apeirian ut eam. Minim altera eu nec, fugit accusam tractatos quo id. Nam facer mucius impetus ei. Eum eu oblique pertinax, vim ea diam inermis insolens. Sea utroque nominati ad, cu principes interpretaris eam, mel et malis semper praesent.\n" +
                "\n" + "Ea exerci civibus mei. Ut nam primis antiopam, vel illum nostrud reprimique ea, nusquam volutpat duo no. Pro hinc erroribus scriptorem at, id mel dicam volutpat euripidis. Luptatum rationibus ut pro, adhuc moderatius appellantur an duo. Dicta elitr petentium est ne, cu noluisse invenire delicata vix. Mel ne debet quidam facilis, id pri ipsum saperet.\n" +
                "\n" + "Dolorem mediocrem dissentiunt in has. Mea ut reque persequeris, an ius sensibus platonem, eirmod dolores eam eu. No quo mazim adipisci voluptatum. Eu vel dicta nominavi corrumpit. Mei cu graece intellegat. Ei iisque.");
        post.setExpirationDate(new Date(2021, 12, 30));
        post.setApproved(true);
        post = postDao.addPost(post);
        
        Tag tag = new Tag();
        tag.setPostId(post.getPostId());
        tag.setTag("#hashtag");
        tag = postDao.addTag(tag);

        Tag newTag = postDao.getTagById(tag.getTagId());
        
        assertEquals(tag, newTag);
    }
    
    @Test
    public void testGetAllTags() {
        User user = new User();
        user.setFirstName("Site");
        user.setLastName("Owner");
        user.setEmail("owner@blog.com");
        user.setRole("Admin");
        user.setPassword("password");
        user = userDao.addUser(user);
        
        Post post = new Post();
        post.setUserId(user.getUserId());
        post.setTitle("Blog 1");
        post.setContent("Lorem ipsum dolor sit amet, pri te integre inermis torquatos, eu mel simul nostrud repudiare, et sanctus legendos his. At has porro similique. No sententiae assueverit mei, ius in integre legimus propriae. Sea quod facete quaerendum ne.\n" +
                "\n" + "Ius amet idque civibus an. Debet aperiam te nec. Ei admodum platonem sea, duo id constituto referrentur. Nec nostrum senserit an. Agam oblique dolores ne mei, dicat mazim adversarium an est.\n" +
                "\n" + "Sit patrioque efficiantur no, sale apeirian ut eam. Minim altera eu nec, fugit accusam tractatos quo id. Nam facer mucius impetus ei. Eum eu oblique pertinax, vim ea diam inermis insolens. Sea utroque nominati ad, cu principes interpretaris eam, mel et malis semper praesent.\n" +
                "\n" + "Ea exerci civibus mei. Ut nam primis antiopam, vel illum nostrud reprimique ea, nusquam volutpat duo no. Pro hinc erroribus scriptorem at, id mel dicam volutpat euripidis. Luptatum rationibus ut pro, adhuc moderatius appellantur an duo. Dicta elitr petentium est ne, cu noluisse invenire delicata vix. Mel ne debet quidam facilis, id pri ipsum saperet.\n" +
                "\n" + "Dolorem mediocrem dissentiunt in has. Mea ut reque persequeris, an ius sensibus platonem, eirmod dolores eam eu. No quo mazim adipisci voluptatum. Eu vel dicta nominavi corrumpit. Mei cu graece intellegat. Ei iisque.");
        post.setExpirationDate(new Date(2021, 12, 30));
        post.setApproved(true);
        post = postDao.addPost(post);
        
        Tag tag = new Tag();
        tag.setPostId(post.getPostId());
        tag.setTag("#hashtag");
        tag = postDao.addTag(tag);
        
        Post post2 = new Post();
        post2.setTitle("Expired Blog");
        post2.setUserId(user.getUserId());
        post2.setContent("Lorem ipsum dolor sit amet, in vitae nostro mea, et mel mollis noluisse signiferumque, at sanctus dolorem corpora pri. Vitae singulis theophrastus ad duo, pri odio soleat an, vis vocibus periculis ut. His in dicat accusamus, eu has partem quodsi. Facer animal intellegat pri ea.\n" +
                "\n" + "Everti gubergren ius ex, et dicunt senserit pri. Duo et aliquam platonem suavitate, omnis facilisi antiopam ex vis, explicari moderatius eum ei. Vix reque consetetur in, cu idque adversarium duo, deserunt reprimique necessitatibus ad has. Scaevola persecuti cu vis. Velit possit semper eos an, doctus scribentur in sit, ius harum facilisis percipitur an.\n" +
                "\n" + "Eum omnis semper et. Dolore principes necessitatibus id mei, sea nisl iisque mandamus ut, perpetua sadipscing ad pri. Id per vocibus scaevola, vim elit civibus eu, in fugit neglegentur usu. Eum eu persius adipiscing, an eam legere essent, mea iriure eruditi no. No etiam melius appetere pri.\n" +
                "\n" + "Est ne solet quaeque, quo eirmod ponderum dissentias an. His no modus senserit, quo an fabulas iudicabit, in dicat dicta veritus cum. Sea illud disputando repudiandae ne. Choro utamur praesent ut his.\n" +
                "\n" + "Corpora invidunt ea pro, nobis platonem ullamcorper ut mea. His ad quis ocurreret, per te erant persecuti. Ex diam eirmod per. Nec at.");
        post2.setExpirationDate(new Date(2021, 11, 30));
        post.setApproved(true);
        post2 = postDao.addPost(post2);
        
        Tag tag2 = new Tag();
        tag2.setPostId(post2.getPostId());
        tag2.setTag("#hashtag2");
        tag2 = postDao.addTag(tag2);

        List<Tag> tags = postDao.getAllTags();
        
        assertEquals(2, tags.size());
        assertTrue(tags.contains(tag));
        assertTrue(tags.contains(tag2));
    }
    
    @Test
    public void testUpdateTag() {
        User user = new User();
        user.setFirstName("Site");
        user.setLastName("Owner");
        user.setEmail("owner@blog.com");
        user.setRole("Admin");
        user.setPassword("password");
        user = userDao.addUser(user);
        
        Post post = new Post();
        post.setTitle("Blog 1");
        post.setUserId(user.getUserId());
        post.setContent("Lorem ipsum dolor sit amet, pri te integre inermis torquatos, eu mel simul nostrud repudiare, et sanctus legendos his. At has porro similique. No sententiae assueverit mei, ius in integre legimus propriae. Sea quod facete quaerendum ne.\n" +
                "\n" + "Ius amet idque civibus an. Debet aperiam te nec. Ei admodum platonem sea, duo id constituto referrentur. Nec nostrum senserit an. Agam oblique dolores ne mei, dicat mazim adversarium an est.\n" +
                "\n" + "Sit patrioque efficiantur no, sale apeirian ut eam. Minim altera eu nec, fugit accusam tractatos quo id. Nam facer mucius impetus ei. Eum eu oblique pertinax, vim ea diam inermis insolens. Sea utroque nominati ad, cu principes interpretaris eam, mel et malis semper praesent.\n" +
                "\n" + "Ea exerci civibus mei. Ut nam primis antiopam, vel illum nostrud reprimique ea, nusquam volutpat duo no. Pro hinc erroribus scriptorem at, id mel dicam volutpat euripidis. Luptatum rationibus ut pro, adhuc moderatius appellantur an duo. Dicta elitr petentium est ne, cu noluisse invenire delicata vix. Mel ne debet quidam facilis, id pri ipsum saperet.\n" +
                "\n" + "Dolorem mediocrem dissentiunt in has. Mea ut reque persequeris, an ius sensibus platonem, eirmod dolores eam eu. No quo mazim adipisci voluptatum. Eu vel dicta nominavi corrumpit. Mei cu graece intellegat. Ei iisque.");
        post.setExpirationDate(new Date(2021, 12, 30));
        post.setApproved(true);
        post = postDao.addPost(post);
        
        Tag tag = new Tag();
        tag.setPostId(post.getPostId());
        tag.setTag("#hashtag");
        Tag originalTag = postDao.addTag(tag);

        tag.setTag("#new");
        postDao.updateTag(tag);
                
        assertNotEquals(originalTag, postDao.getTagById(tag.getTagId()));
    }
    
    @Test
    public void testDeleteTag() {
        User user = new User();
        user.setFirstName("Site");
        user.setLastName("Owner");
        user.setEmail("owner@blog.com");
        user.setRole("Admin");
        user.setPassword("password");
        user = userDao.addUser(user);
        
        Post post = new Post();
        post.setUserId(user.getUserId());
        post.setTitle("Blog 1");
        post.setContent("Lorem ipsum dolor sit amet, pri te integre inermis torquatos, eu mel simul nostrud repudiare, et sanctus legendos his. At has porro similique. No sententiae assueverit mei, ius in integre legimus propriae. Sea quod facete quaerendum ne.\n" +
                "\n" + "Ius amet idque civibus an. Debet aperiam te nec. Ei admodum platonem sea, duo id constituto referrentur. Nec nostrum senserit an. Agam oblique dolores ne mei, dicat mazim adversarium an est.\n" +
                "\n" + "Sit patrioque efficiantur no, sale apeirian ut eam. Minim altera eu nec, fugit accusam tractatos quo id. Nam facer mucius impetus ei. Eum eu oblique pertinax, vim ea diam inermis insolens. Sea utroque nominati ad, cu principes interpretaris eam, mel et malis semper praesent.\n" +
                "\n" + "Ea exerci civibus mei. Ut nam primis antiopam, vel illum nostrud reprimique ea, nusquam volutpat duo no. Pro hinc erroribus scriptorem at, id mel dicam volutpat euripidis. Luptatum rationibus ut pro, adhuc moderatius appellantur an duo. Dicta elitr petentium est ne, cu noluisse invenire delicata vix. Mel ne debet quidam facilis, id pri ipsum saperet.\n" +
                "\n" + "Dolorem mediocrem dissentiunt in has. Mea ut reque persequeris, an ius sensibus platonem, eirmod dolores eam eu. No quo mazim adipisci voluptatum. Eu vel dicta nominavi corrumpit. Mei cu graece intellegat. Ei iisque.");
        post.setExpirationDate(new Date(2021, 12, 30));
        post.setApproved(true);
        post = postDao.addPost(post);
        
        Tag tag = new Tag();
        tag.setPostId(post.getPostId());
        tag.setTag("#hashtag");
        tag = postDao.addTag(tag);

        postDao.deleteTagById(tag.getTagId());
                
        assertNull(postDao.getTagById(tag.getTagId()));
    }
    
    @Test
    public void testGetAllTagsForPost() {
        User user = new User();
        user.setFirstName("Site");
        user.setLastName("Owner");
        user.setEmail("owner@blog.com");
        user.setRole("Admin");
        user.setPassword("password");
        user = userDao.addUser(user);
        
        Post post = new Post();
        post.setUserId(user.getUserId());
        post.setTitle("Blog 1");
        post.setContent("Lorem ipsum dolor sit amet, pri te integre inermis torquatos, eu mel simul nostrud repudiare, et sanctus legendos his. At has porro similique. No sententiae assueverit mei, ius in integre legimus propriae. Sea quod facete quaerendum ne.\n" +
                "\n" + "Ius amet idque civibus an. Debet aperiam te nec. Ei admodum platonem sea, duo id constituto referrentur. Nec nostrum senserit an. Agam oblique dolores ne mei, dicat mazim adversarium an est.\n" +
                "\n" + "Sit patrioque efficiantur no, sale apeirian ut eam. Minim altera eu nec, fugit accusam tractatos quo id. Nam facer mucius impetus ei. Eum eu oblique pertinax, vim ea diam inermis insolens. Sea utroque nominati ad, cu principes interpretaris eam, mel et malis semper praesent.\n" +
                "\n" + "Ea exerci civibus mei. Ut nam primis antiopam, vel illum nostrud reprimique ea, nusquam volutpat duo no. Pro hinc erroribus scriptorem at, id mel dicam volutpat euripidis. Luptatum rationibus ut pro, adhuc moderatius appellantur an duo. Dicta elitr petentium est ne, cu noluisse invenire delicata vix. Mel ne debet quidam facilis, id pri ipsum saperet.\n" +
                "\n" + "Dolorem mediocrem dissentiunt in has. Mea ut reque persequeris, an ius sensibus platonem, eirmod dolores eam eu. No quo mazim adipisci voluptatum. Eu vel dicta nominavi corrumpit. Mei cu graece intellegat. Ei iisque.");
        post.setExpirationDate(new Date(2021, 12, 30));
        post.setApproved(true);
        post = postDao.addPost(post);
        
        Tag tag = new Tag();
        tag.setPostId(post.getPostId());
        tag.setTag("#hashtag");
        tag = postDao.addTag(tag);
        
        Tag tag2 = new Tag();
        tag2.setPostId(post.getPostId());
        tag2.setTag("#hashtag2");
        tag2 = postDao.addTag(tag2);
        
        Post post2 = new Post();
        post2.setUserId(user.getUserId());
        post2.setTitle("Expired Blog");
        post2.setContent("Lorem ipsum dolor sit amet, in vitae nostro mea, et mel mollis noluisse signiferumque, at sanctus dolorem corpora pri. Vitae singulis theophrastus ad duo, pri odio soleat an, vis vocibus periculis ut. His in dicat accusamus, eu has partem quodsi. Facer animal intellegat pri ea.\n" +
                "\n" + "Everti gubergren ius ex, et dicunt senserit pri. Duo et aliquam platonem suavitate, omnis facilisi antiopam ex vis, explicari moderatius eum ei. Vix reque consetetur in, cu idque adversarium duo, deserunt reprimique necessitatibus ad has. Scaevola persecuti cu vis. Velit possit semper eos an, doctus scribentur in sit, ius harum facilisis percipitur an.\n" +
                "\n" + "Eum omnis semper et. Dolore principes necessitatibus id mei, sea nisl iisque mandamus ut, perpetua sadipscing ad pri. Id per vocibus scaevola, vim elit civibus eu, in fugit neglegentur usu. Eum eu persius adipiscing, an eam legere essent, mea iriure eruditi no. No etiam melius appetere pri.\n" +
                "\n" + "Est ne solet quaeque, quo eirmod ponderum dissentias an. His no modus senserit, quo an fabulas iudicabit, in dicat dicta veritus cum. Sea illud disputando repudiandae ne. Choro utamur praesent ut his.\n" +
                "\n" + "Corpora invidunt ea pro, nobis platonem ullamcorper ut mea. His ad quis ocurreret, per te erant persecuti. Ex diam eirmod per. Nec at.");
        post2.setExpirationDate(new Date(2021, 11, 30));
        post.setApproved(true);
        post2 = postDao.addPost(post2);
        
        Tag tag3 = new Tag();
        tag3.setPostId(post2.getPostId());
        tag3.setTag("#hashtag3");
        tag3 = postDao.addTag(tag3);

        List<Tag> tags = postDao.getAllTagsForPost(post.getPostId());
        
        assertEquals(2, tags.size());
        assertTrue(tags.contains(tag));
        assertTrue(tags.contains(tag2));
        assertFalse(tags.contains(tag3));
    }
    
    
}
