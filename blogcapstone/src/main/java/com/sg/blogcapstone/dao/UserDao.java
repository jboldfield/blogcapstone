package com.sg.blogcapstone.dao;

import com.sg.blogcapstone.entity.User;
import java.util.List;

/**
 *
 * @author joldf
 */
public interface UserDao {
    
    User getUserById(int userId);
    
    List<User> getAllUsers();
    
    User addUser(User user);
    
    void updateUser(User user);
    
    void deleteUserById(int userId);
}
