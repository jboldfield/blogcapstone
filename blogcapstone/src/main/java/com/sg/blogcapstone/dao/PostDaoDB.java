package com.sg.blogcapstone.dao;

import com.sg.blogcapstone.entity.Post;
import com.sg.blogcapstone.entity.Tag;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author joldf
 */
@Repository
public class PostDaoDB implements PostDao{
    
    @Autowired
    JdbcTemplate jdbc;
    
    @Autowired
    public PostDaoDB(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    @Override
    public Post getPostById(int postId) {
        try {
            final String SELECT_CONTACT_BY_ID = "SELECT * FROM post WHERE post_id = ?";
            return jdbc.queryForObject(SELECT_CONTACT_BY_ID, new PostMapper(), postId);
        } catch(DataAccessException ex) {
            return null;
        }
    }

    @Override
    public List<Post> getAllPosts() {
        final String SELECT_ALL_POSTS = "SELECT * FROM post";
        return jdbc.query(SELECT_ALL_POSTS, new PostMapper());
    }

    @Override
    public Post addPost(Post post) {
        final String INSERT_POST = "INSERT INTO post(post_id, user_id, title, content, expiration_date, approved) VALUES(?,?,?,?,?,?)";
   
        jdbc.update(INSERT_POST, 
                post.getPostId(), 
                post.getUserId(),
                post.getTitle(),
                post.getContent(),
                post.getExpirationDate(),
                post.getApproved());                                                        
        
        int newId = jdbc.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);
        post.setPostId(newId);
        
        return getPostById(newId);
    }

    @Override
    public void updatePost(Post post) {
        final String UPDATE_POST = "UPDATE post SET user_id = ?, title = ?, content = ?, expiration_date = ?, approved = ? WHERE post_id = ?";
        
        jdbc.update(UPDATE_POST,
                post.getUserId(),
                post.getTitle(),
                post.getContent(),
                post.getExpirationDate(),
                post.getApproved(),
                post.getPostId());
    }

    @Override
    public void deletePostById(int postId) {
        final String DELETE_POST = "DELETE FROM post WHERE post_id = ?";
        
        jdbc.update(DELETE_POST, postId);
    }
    
    @Override
    public List<Tag> getAllTagsForPost(int postId) {
        final String SELECT_TAGS_BY_POSTID = "SELECT * FROM tag WHERE post_id = ?";
        
        List<Tag> tags = jdbc.query(SELECT_TAGS_BY_POSTID, new TagMapper(), postId);
        
        return tags;
    }
    
    @Override
    public Tag getTagById(int tagId) {
        try {
            final String SELECT_TAG_BY_ID = "SELECT * FROM tag WHERE tag_id = ?";
            return jdbc.queryForObject(SELECT_TAG_BY_ID, new PostDaoDB.TagMapper(), tagId);
        } catch(DataAccessException ex) {
            return null;
        }
    }

    @Override
    public List<Tag> getAllTags() {
        final String SELECT_ALL_TAGS = "SELECT * FROM tag";
        return jdbc.query(SELECT_ALL_TAGS, new PostDaoDB.TagMapper());
    }

    @Override
    public Tag addTag(Tag tag) {
        final String INSERT_TAG = "INSERT INTO tag(post_id, tag) VALUES(?,?)";
   
        jdbc.update(INSERT_TAG, 
                tag.getPostId(),
                tag.getTag());                                                        
        
        int newId = jdbc.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);
        tag.setTagId(newId);
        
        return getTagById(newId);
    }

    @Override
    public void updateTag(Tag tag) {
        final String UPDATE_TAG = "UPDATE tag SET post_id = ?, tag = ? WHERE tag_id = ?";
        
        jdbc.update(UPDATE_TAG,
                tag.getPostId(),
                tag.getTag(),
                tag.getTagId());
    }

    @Override
    public void deleteTagById(int tagId) {
        final String DELETE_TAG = "DELETE FROM tag WHERE tag_id = ?";
        
        jdbc.update(DELETE_TAG, tagId);
    }
    
     public static final class PostMapper implements RowMapper<Post> {
        
        @Override
        public Post mapRow(ResultSet rs, int index) throws SQLException {
            Post post = new Post();
            
            post.setPostId(rs.getInt("post_id"));
            post.setUserId(rs.getInt("user_id"));
            post.setTitle(rs.getString("title"));
            post.setContent(rs.getString("content"));
            post.setExpirationDate(rs.getDate("expiration_date"));
            post.setApproved(rs.getBoolean("approved"));
            
            return post;
        }
    }
     
    public static final class TagMapper implements RowMapper<Tag> {
        
        @Override
        public Tag mapRow(ResultSet rs, int index) throws SQLException {
            Tag tag = new Tag();
            
            tag.setTagId(rs.getInt("tag_id"));
            tag.setPostId(rs.getInt("post_id"));
            tag.setTag(rs.getString("tag"));       
            
            return tag;
        }
    }
}
