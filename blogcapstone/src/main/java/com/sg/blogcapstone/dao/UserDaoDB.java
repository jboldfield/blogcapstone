package com.sg.blogcapstone.dao;

import com.sg.blogcapstone.entity.User;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author joldf
 */
@Repository
public class UserDaoDB implements UserDao{
    
    @Autowired
    JdbcTemplate jdbc;
    
    @Autowired
    public UserDaoDB(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }
    
    @Override
    public User getUserById(int userId) {
        try {
            final String SELECT_USER_BY_ID = "SELECT * FROM user WHERE user_id = ?";
            return jdbc.queryForObject(SELECT_USER_BY_ID, new UserMapper(), userId);
        } catch(DataAccessException ex) {
            return null;
        }
    }

    @Override
    public List<User> getAllUsers() {
        final String SELECT_ALL_USERS = "SELECT * FROM user";
        return jdbc.query(SELECT_ALL_USERS, new UserMapper());
    }

    @Override
    @Transactional
    public User addUser(User user) {
        final String INSERT_USER = "INSERT INTO user(user_id, first_name, last_name, email, role, password) VALUES(?,?,?,?,?,?)";

        jdbc.update(INSERT_USER,
                user.getUserId(),
                user.getFirstName(),
                user.getLastName(),
                user.getEmail(),
                user.getRole(),
                user.getPassword());
        
        int newId = jdbc.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);
        user.setUserId(newId);
        
        return user;
    }

    @Override
    public void updateUser(User user) {
        final String UPDATE_USER = "UPDATE user SET first_name = ?, last_name = ?, email = ?, role = ?, password = ? WHERE user_id = ?";
        
        jdbc.update(UPDATE_USER,
                user.getFirstName(),
                user.getLastName(),
                user.getEmail(),
                user.getRole(),
                user.getPassword(),
                user.getUserId()); 
    }

    @Override
    public void deleteUserById(int userId) {
        
        final String DELETE_USER = "DELETE FROM user WHERE user_id = ?";
        jdbc.update(DELETE_USER, userId);
    }
    
    public static final class UserMapper implements RowMapper<User> {
        
        @Override
        public User mapRow(ResultSet rs, int index) throws SQLException {
            User user = new User();
            
            user.setUserId(rs.getInt("user_id"));
            user.setFirstName(rs.getString("first_name"));
            user.setLastName(rs.getString("last_name"));
            user.setEmail(rs.getString("email"));
            user.setRole(rs.getString("role"));
            user.setPassword(rs.getString("password"));
            
            return user;
        }
    }
}
