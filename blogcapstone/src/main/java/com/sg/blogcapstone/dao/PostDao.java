package com.sg.blogcapstone.dao;

import com.sg.blogcapstone.entity.Post;
import com.sg.blogcapstone.entity.Tag;
import java.util.List;

/**
 *
 * @author joldf
 */
public interface PostDao {
    
    Post getPostById(int postId);
    
    List<Post> getAllPosts();
    
    Post addPost(Post post);
    
    void updatePost(Post post);
    
    void deletePostById(int postId);
    
    List<Tag> getAllTagsForPost(int postId);
    
    Tag getTagById(int tagId);
    
    List<Tag> getAllTags();
    
    Tag addTag(Tag tag);
    
    void updateTag(Tag tag);
    
    void deleteTagById(int tagId);
}
