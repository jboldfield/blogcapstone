package com.sg.blogcapstone.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sg.blogcapstone.entity.Post;
import com.sg.blogcapstone.entity.Tag;
import com.sg.blogcapstone.entity.User;
import com.sg.blogcapstone.service.BlogcapstoneServiceLayer;



@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/home")

public class Controller {
	
    @Autowired
    public BlogcapstoneServiceLayer serv;
    public Post post;
    public Tag tag;
    public User user;


    public Controller(BlogcapstoneServiceLayer serv) {

        this.serv=serv;
    }




    @GetMapping("/index")
     @ResponseStatus(HttpStatus.CREATED)
     public ResponseEntity<List<Post>> homeblog()  {

        List<Post> all_post = serv.getValidPosts();
    	
        return ResponseEntity.ok(all_post);
     }

    
    @PostMapping("/new")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<List<Post>> new_post(@RequestBody Post po)  {

      // List<Post> all_post = serv.getNonExpiredPosts();
    	
    	serv.addPost(po);
    	return null;
   	
      // return ResponseEntity.ok(all_post);
    }



@PostMapping("/tag")
@ResponseStatus(HttpStatus.CREATED)
public ResponseEntity<List<Post>> new_tag(@RequestBody Tag tag)  {

  // List<Post> all_post = serv.getNonExpiredPosts();
	
	serv.addTag(tag);
	return null;
	
  // return ResponseEntity.ok(all_post);
}



@PostMapping("/post/{id}")
public ResponseEntity<Post>  findPost(@PathVariable int id) {

       //Vehicle car=serv.getVehicleByID(id);
	
	Post po=serv.getPostById(id);
    return ResponseEntity.ok(po);
}



@PostMapping("/edit")
public ResponseEntity<Post>  editPost(@RequestBody Post po) {

       //Vehicle car=serv.getVehicleByID(id);
	
	serv.updatePost(po);
    return ResponseEntity.ok(po);
}


@GetMapping("/posts")
public ResponseEntity<List<Post>>  findallpost() {

       //Vehicle car=serv.getVehicleByID(id);
	
	List<Post> po=serv.getAllPosts();
    return ResponseEntity.ok(po);
}


@GetMapping("/tags")
public ResponseEntity<List<Tag>>  findalltags() {

       //Vehicle car=serv.getVehicleByID(id);
	
	List<Tag> po=serv.getAllTags();
    return ResponseEntity.ok(po);
}



@PostMapping("/tags/{id}")
public ResponseEntity<List<Tag>>  get_tags_from_post(@PathVariable int id) {

       //Vehicle car=serv.getVehicleByID(id);
	
	List<Tag> po=serv.getAllTagsForPost(id);
    return ResponseEntity.ok(po);
}


@GetMapping("/users")
public ResponseEntity<List<User>>  findalluser() {

       //Vehicle car=serv.getVehicleByID(id);
	
	List<User> po=serv.findalluser();
    return ResponseEntity.ok(po);
}



}



     // missing function on the service layer  


