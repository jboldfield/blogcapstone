package com.sg.blogcapstone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlogcapstoneApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlogcapstoneApplication.class, args);
	}

}
