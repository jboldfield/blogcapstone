/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.blogcapstone.service;

import com.sg.blogcapstone.entity.Post;
import com.sg.blogcapstone.entity.Tag;
import com.sg.blogcapstone.entity.User;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Riddle
 */
public interface BlogcapstoneServiceLayer {
    List<Post> getValidPosts();
    
    Post getPostById(int postId);
    
    List<Post> getAllPosts();
    
    Post addPost(Post post);
    
    void updatePost(Post post);
    
    void deletePostById(int postId);
    
    List<Tag> getAllTagsForPost(int postId);
    
    Tag getTagById(int tagId);
    
    List<Tag> getAllTags();
    
    Tag addTag(Tag tag);
    
    void updateTag(Tag tag);
    
    void deleteTagById(int tagId);
    
    List<User> findalluser();
}
