/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.blogcapstone.service;

import com.sg.blogcapstone.dao.PostDaoDB;
import com.sg.blogcapstone.dao.UserDao;
import com.sg.blogcapstone.entity.Post;
import com.sg.blogcapstone.entity.Tag;
import com.sg.blogcapstone.entity.User;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Riddle
 */
@Component
public class BlogcapstoneServiceLayerImpl implements BlogcapstoneServiceLayer{
	
    @Autowired
    PostDaoDB dao;
    @Autowired
    UserDao userdb;
    
    public List<Post> getValidPosts()
    {
        List<Post> posts = dao.getAllPosts();
        //New date objects should be initialized with current date
        return posts.stream().filter((p) -> p.getExpirationDate().compareTo(new Date()) > 0 && p.getApproved()).collect(Collectors.toList());
    }
    
    public Post getPostById(int postId)
    {
        return dao.getPostById(postId);
    }
    
    public List<Post> getAllPosts()
    {
        return dao.getAllPosts();
    }
    
    public Post addPost(Post post)
    {
        return dao.addPost(post);
    }
    
    public void updatePost(Post post)
    {
        dao.updatePost(post);
    }
    
    public void deletePostById(int postId)
    {
        dao.deletePostById(postId);
    }
    
    public List<Tag> getAllTagsForPost(int postId)
    {
        return dao.getAllTagsForPost(postId);
    }
    
    public Tag getTagById(int tagId)
    {
        return dao.getTagById(tagId);
    }
    
    public List<Tag> getAllTags()
    {
        return dao.getAllTags();
    }
    
    public Tag addTag(Tag tag)
    {
        return dao.addTag(tag);
    }
    
    public void updateTag(Tag tag)
    {
        dao.updateTag(tag);
    }
    
    public void deleteTagById(int tagId)
    {
        dao.deleteTagById(tagId);
    }
    
    public List<User> findalluser(){
    	
    	return userdb.getAllUsers();
    }
}
