DROP DATABASE IF EXISTS BlogCapstone;
CREATE DATABASE BlogCapstone;
USE BlogCapstone;

CREATE TABLE user(
    user_id INT PRIMARY KEY AUTO_INCREMENT,
    first_name VARCHAR(25) NOT NULL,
    last_name VARCHAR(25) NOT NULL,
    email VARCHAR(50) NOT NULL,
    role VARCHAR(15) NOT NULL,
    password VARCHAR(50) NOT NULL
);

CREATE TABLE post(
    post_id INT PRIMARY KEY AUTO_INCREMENT,
    user_id INT NOT NULL,
    title VARCHAR(100) NOT NULL,
    content LONGTEXT NOT NULL,
    expiration_date DATE NOT NULL,
    approved BOOLEAN NOT NULL,
    FOREIGN KEY (user_id) REFERENCES user(user_id)
);

CREATE TABLE tag(
    tag_id INT PRIMARY KEY AUTO_INCREMENT,
    post_id INT NOT NULL,
    tag VARCHAR(50) NOT NULL,
    FOREIGN KEY (post_id) REFERENCES post(post_id)
);

INSERT INTO user VALUES
    (1, "Site", "Owner", "owner@blog.com", "Admin", "password"),
    (2, "Blogger", "1", "blogger1@blog.com", "Blogger", "password");

INSERT INTO post VALUES
    (1, 1, "Blog 1", 
    "Lorem ipsum dolor sit amet, pri te integre inermis torquatos, eu mel simul nostrud repudiare, et sanctus legendos his. At has porro similique. No sententiae assueverit mei, ius in integre legimus propriae. Sea quod facete quaerendum ne.

Ius amet idque civibus an. Debet aperiam te nec. Ei admodum platonem sea, duo id constituto referrentur. Nec nostrum senserit an. Agam oblique dolores ne mei, dicat mazim adversarium an est.

Sit patrioque efficiantur no, sale apeirian ut eam. Minim altera eu nec, fugit accusam tractatos quo id. Nam facer mucius impetus ei. Eum eu oblique pertinax, vim ea diam inermis insolens. Sea utroque nominati ad, cu principes interpretaris eam, mel et malis semper praesent.

Ea exerci civibus mei. Ut nam primis antiopam, vel illum nostrud reprimique ea, nusquam volutpat duo no. Pro hinc erroribus scriptorem at, id mel dicam volutpat euripidis. Luptatum rationibus ut pro, adhuc moderatius appellantur an duo. Dicta elitr petentium est ne, cu noluisse invenire delicata vix. Mel ne debet quidam facilis, id pri ipsum saperet.

Dolorem mediocrem dissentiunt in has. Mea ut reque persequeris, an ius sensibus platonem, eirmod dolores eam eu. No quo mazim adipisci voluptatum. Eu vel dicta nominavi corrumpit. Mei cu graece intellegat. Ei iisque.",
 "2021-12-30", true),
    (2, 1, "Expired Blog",
    "Lorem ipsum dolor sit amet, in vitae nostro mea, et mel mollis noluisse signiferumque, at sanctus dolorem corpora pri. Vitae singulis theophrastus ad duo, pri odio soleat an, vis vocibus periculis ut. His in dicat accusamus, eu has partem quodsi. Facer animal intellegat pri ea.

Everti gubergren ius ex, et dicunt senserit pri. Duo et aliquam platonem suavitate, omnis facilisi antiopam ex vis, explicari moderatius eum ei. Vix reque consetetur in, cu idque adversarium duo, deserunt reprimique necessitatibus ad has. Scaevola persecuti cu vis. Velit possit semper eos an, doctus scribentur in sit, ius harum facilisis percipitur an.

Eum omnis semper et. Dolore principes necessitatibus id mei, sea nisl iisque mandamus ut, perpetua sadipscing ad pri. Id per vocibus scaevola, vim elit civibus eu, in fugit neglegentur usu. Eum eu persius adipiscing, an eam legere essent, mea iriure eruditi no. No etiam melius appetere pri.

Est ne solet quaeque, quo eirmod ponderum dissentias an. His no modus senserit, quo an fabulas iudicabit, in dicat dicta veritus cum. Sea illud disputando repudiandae ne. Choro utamur praesent ut his.

Corpora invidunt ea pro, nobis platonem ullamcorper ut mea. His ad quis ocurreret, per te erant persecuti. Ex diam eirmod per. Nec at.",
    "2021-11-30", true),
    (3, 2, "Blog 3",
    "Lorem ipsum dolor sit amet, mei te alienum imperdiet, eam vidisse laboramus ne. Te vitae oblique similique eam. Unum iisque facilis usu ei, has et simul deserunt reprimique, te mei wisi commune. Sumo graecis suavitate sit no, principes scribentur cum et. Pri ex paulo suscipit deserunt, duis dolorem cu qui. Te eos labore quaerendum.

Eam congue fabulas vivendo an. Vel vide illud id, mei et ceteros accommodare. Alienum consetetur eu pro. Pri te eligendi sensibus postulant, diam eloquentiam sea cu, has cu noster albucius appareat.

Nam unum ferri consulatu at. Habemus tincidunt assueverit cu nam, facer facete nam te. Ex erat idque dicunt nam, ut placerat urbanitas has. Quas reque at quo.

Cum te diam postulant, eam no vero quaestio contentiones. Id has mucius melius, saepe commune id est, paulo perfecto mel no. Nam tollit necessitatibus id. Mei magna lobortis ut, et qui aeque iuvaret nusquam, quis duis adhuc pro ex. Eu hinc everti pertinacia vim, ea nusquam quaestio eum. Ea est quando alterum alienum.

Sumo novum laoreet vim et, cu aliquip constituam eum. Ius eu augue dictas accommodare. Vis hinc ignota cu. Id graeco temporibus est, ipsum animal apeirian eos ut, nam utinam lobortis posidonium id. Ut sea dicunt.",
    "2023-12-29", true),
    (4, 2, "Blog 4",
    "Lorem ipsum dolor sit amet, mea ut discere elaboraret sadipscing, ad mel option sanctus hendrerit. Ea vivendum appellantur consectetuer vel, summo graece eligendi mel id. Eleifend sapientem at est, te rebum mucius posidonium mel. Cu tantas consulatu reprimique eam, ad esse phaedrum nec. Iisque mandamus adipiscing an vel, ex ceteros liberavisse quo.

Usu esse dictas ex. Nostrud ornatus eos at. Mel in justo aperiam invidunt. Fabulas eligendi facilisis pro ea, mel malis nonumy fierent et. No esse consetetur scripserit mei, sit enim eripuit omnesque ea.

Ad oratio primis cum. Sea commune nominati te. Id discere vocibus his, cu vim aeterno dolorum. Eam in autem iudicabit consetetur, vim ad iisque mentitum appareat, enim vidit copiosae sed at. Ei consetetur elaboraret scriptorem duo, error aliquid eam an.

Et illum augue graeci vim, eu elitr phaedrum salutandi vix. Duo nemore placerat temporibus cu. Id eum amet facer. Duo veri virtute instructior ea, cu eos alii iriure quaerendum, solum habemus intellegebat pri at. Doming epicuri ius id, qui veniam scripta at.

Mei in inimicus molestiae, sit an adhuc consectetuer. Illud dignissim euripidis sed ex, no decore dicunt iudicabit sed, veri postulant ex nec. Omnis intellegebat quo ea, eum graeco officiis reprimique ut. Eum viderer euismod mnesarchum id. Porro virtute ius ea, pro malis postea et.

Cu nam prima insolens petentium, mei labore impedit inciderint cu, ius modo facilisis contentiones an. Quo labore tritani interpretaris at, mei cu probo movet viderer. Sed eius augue minimum ad, est an nobis philosophia. Ius eu fierent posidonium, viris adipisci his ea. Id cibo etiam nostrum vel.

Rebum molestiae at quo. Eruditi philosophia te sea. Id accumsan petentium incorrupte vim, in augue putant sensibus mel. Tempor bonorum ullamcorper his eu.

Mazim error his eu, eam ut laudem propriae sensibus. Magna ponderum liberavisse ius in. Ut mea vero novum partiendo.",
    "2023-12-29", false);

INSERT INTO tag VALUES 
    (1, 1, "first"),
    (2, 1, "blog"),
    (3, 1, "notexpired"),
    (4, 2, "second"),
    (5, 2, "expired"),
    (6, 3, "third"),
    (7, 4, "fourth"),
    (8, 4, "unapproved");

SELECT * from tag;
SELECT * from post;